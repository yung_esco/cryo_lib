#include <posapi.h>
#include <posapi_all.h>

#ifndef APP_CRYO_NETWORK
#define APP_CRYO_NETWORK 1

#include "cryo_std.h"

#if defined(__cplusplus)
extern "C" {
#endif

	/// M�thodes d'appels HTTP
	#define CRYO_POST_METHOD			"POST"
	#define CRYO_GET_METHOD				"GET"
	#define CRYO_PUT_METHOD				"PUT"
	#define CRYO_DELETE_METHOD			"DELETE"
	#define CRYO_PATCH_METHOD			"PATCH"

	#define CRYO_TOKEN_HTTP_VER			"HTTP/1.1 "
	#define CRYO_TOKEN_SERVER_NAME		"Server: "
	#define CRYO_TOKEN_SERVER_DATE		"Date: "
	#define CRYO_TOKEN_CONTENT_TYPE		"Content-Type: "
	#define CRYO_CRLF					"\r\n"
	#define CRYO_CRLF2					"\r\n\r\n"

	/// Gestion des erreurs r�seaux
	#define	CRYO_ERR_CREATE_SOCKET		1000
	#define	CRYO_ERR_SOCK_ADDR_SET		1001
	#define	CRYO_ERR_NET_CONNECT		1002
	#define	CRYO_ERR_CONNECT_API		1003
	#define CRYO_ERR_NETWORK			-1000

	typedef int CRYO_SOCKET;

	/// D�marrage de la connexion au r�seau
	bool Cryo_InitWireless();

	/// Connexion PPP au r�seau
	bool Cryo_PPPLogin(const uchar* apn,
		const uchar* uid, const uchar* upass);

	/// Cr�e une socket pour une connexion TCP
	/// Renvoie true en cas de suuc�s et false
	/// en cas d'�chec. Le pointeur CRYO_SOCKET pass� en 
	/// param�tre est modifi� en cas r�ussite. En cas
	/// d'�chec, le code d'erreur est stock� dans error
	bool Cryo_CreateTcpSocket(CRYO_SOCKET*, int* error);

	/// Associe une adresse ip � une structure de type sock_addr
	/// En cas d'erreur, le code est stock� dans la variable error
	bool Cryo_InitServerAddr(NET_SOCKADDR*, cstring serverIp,
		const int port, int* error);

	/// D�marre la connexion du tpe au serveur pass� en deuxi�me param�tre
	/// sur le port pass� en troisi�me param�tre. Modifie le premier
	/// param�tre avec l'id de la socket g�n�r�e. Renvoie true si
	/// la connexion a r�ussi et false dans le cas contraire
	bool Cryo_TcpConnect(CRYO_SOCKET*, cstring server,
		const int port, int* error);

	/// Emet une requete tcp vers un serveur
	/// Retourne le nombre d'octets �crits ou 
	/// une valeur n�gative en cas d'�chec le la requete
	int Cryo_SendTcpRequest(const CRYO_SOCKET, const uchar* body);

	/// Lit le retour du serveur � partir de la socket
	/// Ecris (au maximum) le nombre d'octets pass�s en troisi�me param�tre
	/// dans le buffer pass� en deuxi�me param�tre
	/// Renvoie le nombre d'octets lus ou une valeur n�gative en cas d'erreur
	int Cryo_ReadTcpResponse(const CRYO_SOCKET, uchar* out, const int outLen);

#if defined(__cplusplus)
}
#endif

#endif

