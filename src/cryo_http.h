#ifndef APP_CRYO_HTTP
#define APP_CRYO_HTTP 1

#include "cryo_std.h"

#if defined(__cplusplus)
extern "C" {
#endif

	/// Repr�sentation d'une r�ponse http selon le framework Cryo
	typedef struct {

		int code;
		uchar server[100];
		uchar date[40];
		uchar contentType[100];
		void * body; // initialis�e dynamiquement
		bool initialized;

	} CRYO_HTTP_RESPONSE;

	/// Cr�e un ent�te http � partir des valeurs pass�es en param�tre
	/// L'ent�te est �crit dans le param�tre buffer
	/// En cas de r�ussite la fonction retourne true,
	/// false dans le cas contraire
	/// N'oubliez surtout pas de lib�rer le buffer apr�s utilisation
	bool Cryo_BuildHttpHeader(char **buffer, cstring method,
		cstring contentType, cstring host, cstring path,
		cstring headers, cstring body);

	/// Met � z�ro le response et le rend pr�t utilisation
	void Cryo_InitHttpResponse(CRYO_HTTP_RESPONSE *);

	/// Lib�re un objet de type http response
	/// Cette m�thode doit toujours �tre appel�e pour lib�rer
	/// le body initialis� dynamiquement dans la structure
	void Cryo_ReleaseHttpResponse(CRYO_HTTP_RESPONSE *);

	/// Extrait les composantes de la r�ponse http
	CRYO_HTTP_RESPONSE Cryo_ParseHttpResponse(cstring,
		bool (*)(cstring, CRYO_HTTP_RESPONSE * const));

#if defined(__cplusplus)
}
#endif

#endif

