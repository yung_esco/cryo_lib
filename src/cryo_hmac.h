#ifndef APP_HMAC
#define APP_HMAC 1

#if defined(__cplusplus)
extern "C"
{
#endif

	/// Calcule le hashmac sha1 du message pass� en param�tre
	bool Cryo_HashmacSha1(const char * message, const char * secretKey, char hash[21]);

#if defined(__cplusplus)
}
#endif

#endif

