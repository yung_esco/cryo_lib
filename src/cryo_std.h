#ifndef APP_CRYO_STD
#define APP_CRYO_STD 1

#if defined(__cplusplus)
extern "C"
{
#endif

	/// Codes couleur
	#define CRYO_COLOR_ERROR	0xFFFF0000
	#define CRYO_COLOR_INFO		0xFF0000FF
	#define CRYO_COLOR_SUCCESS	0xFF00FF00

	/// Types personnalis�s

	typedef const char*		cstring;
	typedef char*			string;

	#define bool	int
	#define true	1
	#define false	0

	#define intptr_t	unsigned int
	#define ptrint_t	intptr_t

	/* define unsigned 8-bit type if not available in stdint.h */
	#if !defined(UINT8_MAX)
		typedef unsigned char uint8_t;
	#endif

	/* define unsigned 16-bit type if not available in stdint.h */
	#if !defined(UINT16_MAX)
		typedef unsigned short uint16_t;
	#endif

	#define li_32(h) 0x##h##u
	#if !defined(UINT32_MAX)
		 typedef unsigned int uint32_t;
	#endif
	/// Autres
	#define MAX_ATTEMPTS 5
	#define SECOND 1000
	#define BASIC_DELAY 2500
	/// Cr�e un timestamp
	void Cryo_GetTimeStamp(char time[15]);

	/// Lib�re un bloc de m�moire et r�ecris l'espace avec des 0
	void Cryo_Free(void *);

	/// Affiche un message color� � l'�cran
	/// La couleur est sous la forme 0xFFRRGGBB
	void Cryo_PrintColoredMsg(const char * msg, unsigned int color);

	/// Affiche un message de confirmation (vert)
	void Cryo_PrintSuccessMessage(const char *);

	/// Affiche un message informatif (bleu)
	void Cryo_PrintInfoMessage(const char *);

	/// Affiche un message d'erreur
	void Cryo_PrintErrorMessage(const char *);

#if defined(__cplusplus)
}
#endif

#endif

