#include <posapi.h>
#include <posapi_all.h>
#include <stddef.h>

#ifndef APP_CRYO_JSON
#define APP_CRYO_JSON 1

#include "cryo_std.h"

#ifdef __cplusplus
extern "C" {
#endif

	bool Cryo_ParseJson(const char * json, void * out,
		bool (*parser)(const char * key, void * val, int type, void * obj),
		bool endOnError);
	
#ifdef __cplusplus
}
#endif

#endif