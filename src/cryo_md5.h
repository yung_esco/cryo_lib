#ifndef APP_CRYO_MD5
#define APP_CRYO_MD5

#define MD5_RAW_DIGEST_SIZE			16
#define MD5_STR_DIGEST_SIZE			32

#if defined(__cplusplus)
extern "C" {
#endif
    /// Calcule la somme md5 de la chaine pass�e en param�tre
    /// et l'�crit dans out
    /// len repr�sente la longueur de la chaine d'entr�e
    void Cryo_MD5(const uint8_t* out,
        uint8_t* result,
        size_t resultLength);

    /// Convertit une sortie md5 en sa version hexad�cimale
    void Cryo_ConvertMd5ToHex(char out[MD5_STR_DIGEST_SIZE + 1],
        char hash[MD5_RAW_DIGEST_SIZE + 1]);

#if defined(__cplusplus)
}
#endif

#endif // !APP_CRYO_MD5
