#ifndef APP_SHA1
#define APP_SHA1 1

#include <posapi.h>
#include <posapi_all.h>

#include "cryo_std.h"

#define SHA1_BLOCK_SIZE				64
#define SHA1_DIGEST_SIZE			20
#define SHA1_DIGEST_HEXVAL_SIZE		40

#if defined(__cplusplus)
extern "C"
{
#endif

	/// Calcule la somme sha1 de la chaine pass�e en param�tre
	/// et l'�crit dans out
	/// len repr�sente la longueur de la chaine d'entr�e
	void Cryo_SHA1(
		char *out,
		const char *str,
		unsigned int len);

	/// Convertit une sortie sha1 en sa version hexad�cimale
	void Cryo_ConvertSha1ToHex(char out[SHA1_DIGEST_HEXVAL_SIZE + 1],
		char hash[SHA1_DIGEST_SIZE + 1]);

#if defined(__cplusplus)
}
#endif

#endif