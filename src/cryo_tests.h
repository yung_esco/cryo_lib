#ifndef APP_CRYO_TESTS
#define APP_CRYO_TESTS 1

#include "cryo_std.h"

#if defined(__cplusplus)
extern "C"
{
#endif

	/// V�rifie une assertion d'egalite concernant les deux chaines de caract�res pass�es
	/// en param�tre. Retourne true ou false. Ne stoppe pas l'ex�cution du programme en
	/// cas d'�chec
	bool Cryo_Assert_Str(const char *, const char *);

	/// V�rifie une assertion d'�galit� entre deux entiers. Retourne true ou false
	/// Ne stoppe pas l'ex�cution du programme en cas d'�chec
	bool Cryo_Assert_Int(int, int);

#if defined(__cplusplus)
}
#endif

#endif

